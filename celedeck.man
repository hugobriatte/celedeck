.\" Manpage for celedeck.
.TH celedeck 8 "Unknow date" "In Dev" "celedeck man page"
.SH NAME
celedeck \- Convert plain text descripting a card into a image
.SH SYNOPSIS
celedeck [FILE | -d DIR]
.SH DESCRIPTION
Celedeck is a tool to create quickly card for prototype.

Celedeck reads in the standard input. It reads first the title, then the description, and as many subtitle/subdescription as needed.

It output then the result onto a FILE or in the stdout as png data.

Basically, it creates a commandline for image magick. In fact, you will need to have ImageMagick installed onto your computer.

.SH OPTIONS
.IP "-d DIR"
Treat all file in the specified directory excluding all files that finishes with '.png'. The file will be written in the same directory.
.IP "-w WIDTH"
Change the width of the card
.IP "-h HEIGH"
Change the heigh of the card
.IP "-c COLOR"
Change the background color of the card. Color should be recognizable by Image Magick, or hexadecimal (#xxxxxx)
.IP "-t COLOR"
Change the text color of the card (as for the background)

.SH AUTHOR
Hugo Briatte (hugobriatte@hotmail.fr)
